/*
   program: a3Primer.c
   date:    november 3, 2014
   author:  danny abesdris
   purpose: primer for ipc144 (fall 2014) assignment #3
      including solution to the
      void genSudokuNumbers(int grid[ ]) function.

      *** UPDATED VERSION (1.1) posted on Nov 18, 2014 ***
    Update courtesy of: Joseph Hughes
*/

#include <stdio.h>
#include <time.h>
#include <stdlib.h>
#include <time.h>

#define N 9
/* NOTE: You will have to code the 2 functions BELOW "checkDups" and "clearArray"
   before running this program to see the generated output.
*/
int checkDups(int [ ], int, int);
void clearArray(int [ ], int);

int genSudokuNumbers(int grid[ ]); /* already coded */

void genSudokuBoard(int grid[ ], int display[ ]);
void displaySudokuBoard(int display[ ], int bigOh);
void getRandNum(int low, int high, int *pn);
void printSudokuBoardToFile(int display[ ], const char *fileName);

int main( ) {
   int i, grid[N*N];
   int loops;
   int display[N*N];
   char fileName[] = "FILE";
   srand(time(NULL));

   loops = genSudokuNumbers(grid); /* students must add code to return the correct
              value for all instructions executed in all loops */

   displaySudokuBoard(grid, loops);

   genSudokuBoard(grid, display);

   printSudokuBoardToFile(display, fileName);

   return 0;
}

/* given an array of 'n' elements, this function determines if the
   value 'num' is already present within the array 'nums' and if
   so, sends back a true value (1) or false (0) otherwise */
int checkDups(int nums[ ], int search, int n) {
   while (n-- > 0) {
      if (search == nums[n]) {
         return 1;
      }
   }
  return 0;
}

/* Sets all 'n' values in the 'nums' array to 0 */
void clearArray(int nums[ ], int n) {
   while (n-- > 0) {
      nums[n] = 0;
   }
}

int genSudokuNumbers(int grid[ ]) {
   int c, i, j, rowNum, colNum, blockNum;
   int test[N], dup, temp, valid, cnt, iterations=0;

   srand(time(NULL));         /* seeding the random number generator */
   for(i=0; i<N*N; i++) {     /* initializing the grid array to all 0's */
      grid[i] = 0;
   }

   for(c=0; c<N*N; c++) {     /* main loop to generate N*N numbers for the grid */
      getRandNum(1, N, &temp); /* generate random # from 1 to N inclusive */

      valid = dup = cnt = 0;
      while(!valid) {         /* keep looping as long as the 'valid' flag is false */
         clearArray(test, N);
         /* the calculations below determine the row, col,
            and block numbers (index 0 to 8) based on the
            linear index variable 'c' */
         rowNum = c / N;
         colNum = c % N;
         blockNum = (rowNum / 3) * 3 + (colNum / 3);

         /* now check to see if the number 'temp' is a
            duplicate in the row, column, and block to which
            'c' corresponds */

         for(j=0; j<colNum; j++) {    /* fill row (but only up to colNum) */
            test[j] = grid[rowNum*N+j];
         }
         dup += checkDups(test, temp, colNum);

         if(!dup) {                   /* row is valid, now check column */
            clearArray(test, N);
            for(j=0; j<rowNum; j++) { /* fill column (but only up to rowNum) */
               test[j] = grid[j*N + colNum];
            }
            dup += checkDups(test, temp, rowNum);

            if(!dup) {                /* column is valid now check block */
               clearArray(test, N);

               for(j=0; j<N; j++) {
                  test[j] = grid[((blockNum/3)*N*3) + (colNum/3)*3 + (j/3)*N + j%3];
               }
               /* equation used to generate array
                  coordinates for all N blocks
                  (i.e.)
                  0, 1, 2,   9, 10, 11,  18, 19, 20  {blk 0}
                  3, 4, 5,  12, 13, 14,  21, 22, 23  {blk 1}
                  6, 7, 8,  15, 16, 17,  24, 25, 26  {blk 2}

                  27, 28, 29, 36, 37, 38, 45, 46, 47 {blk 3}
                  30, 31, 32, 39, 40, 41, 48, 49, 50
                  33, 34, 35, 42, 43, 44, 51, 52, 53

                  54, 55, 56, 63, 64, 65, 72, 73, 74
                  57, 58, 59, 66, 67, 68, 75, 76, 77
                  60, 61, 62, 69, 70, 71, 78, 79, 80 {blk 8}
               */
               dup += checkDups(test, temp, N);
            }
         }
         if(!dup) { /* no duplicates in row, column, or block, so number
                       can be inserted into the grid */
            grid[c] = temp;
            valid = 1;
            cnt = 0;
         }
         else {     /* duplicate number found, so reset flags and generate
                       new random number */
            temp = rand( ) % N + 1;
            dup = 0;
            cnt++;
         }
         if(cnt > N*N) {
            /* if after N*N attempts, no possible value is found
               then reset the entire array and start over
               (brute force algorithm)
               average runtime: 500000 iterations */
            dup = cnt = 0;
            valid = 1;
            clearArray(grid, N*N);
            temp = rand( ) % N + 1;
            c = -1; /* will be reset to 0 in for loop */
         }
      }
      iterations++;
   }
   return iterations; /* 'iterations' must be updated within this function BEFORE it is returned */
}

void genSudokuBoard(int grid[ ], int display[ ]) {
   int M = 4;
   int blockNum;
   int originNum = 0;
   int cell;
   int base[] = { 0, 1, 2, 9, 10, 11, 18, 19, 20 };
   int s, c, t, randNum, pos;

   for (blockNum=0; blockNum<N; blockNum++) { /* iterate over blocks */

      /* get the first number in a block */
      if (blockNum > 0) {
         originNum += blockNum % 3 ? 3 : 21;
      }

      /* shuffle base */
      for (s=0; s<N; s++) {
         getRandNum(0, N-1, &randNum);
         t = base[randNum];
         base[randNum] = base[s];
         base[s] = t;
      }

      /* copy first M shuffled numbers from block */
      for (c=0; c<M; c++) {
         pos = base[c] + originNum;
         display[pos] = grid[pos];
      }
   }
}

void displaySudokuBoard(int display[ ], int bigOh) {
   int i;
   printf("PLAY IPC144 SUDOKU");

   for (i=0; i<N*N; i++) {  /* displaying all N*N numbers in the 'grid' array */
      if (i%9==0) {
         printf("\n");
      }
      if (i%27==0) {
         printf("+-----+-----+-----+\n");
      }
      if (i%9==0) {
         printf("|");
      }
      if (i%3!=0) {
         printf(" ");
      }
      if (i%3==0 && i%9!=0) {
         printf("|");
      }

      printf("%d", display[i]);

      if (i%9==8) {
         printf("|");
      }
   }

   printf("\n+-----+-----+-----+");
   printf("\nTotal Instructions:%d\n", bigOh);
}

void getRandNum(int low, int high, int *pn) {
   *pn = rand( ) % high + low;
}

void printSudokuBoardToFile(int display[ ], const char *fileName) {
   int i;
   FILE *f = fopen(fileName, "w");
   fprintf(f, "PLAY IPC144 SUDOKU");

   for (i=0; i<N*N; i++) {  /* displaying all N*N numbers in the 'grid' array */
      if (i%9==0) {
         fprintf(f, "\n");
      }
      if (i%27==0) {
         fprintf(f, "+-----+-----+-----+\n");
      }
      if (i%9==0) {
         fprintf(f, "|");
      }
      if (i%3!=0) {
         fprintf(f, " ");
      }
      if (i%3==0 && i%9!=0) {
         fprintf(f, "|");
      }

      if (display[i] > 0) {
         fprintf(f, "%d", display[i]);
      } else {
         fprintf(f, " ");
      }

      if (i%9==8) {
         fprintf(f, "|");
      }
   }

   fprintf(f, "\n+-----+-----+-----+\n");
   fclose(f);
}
